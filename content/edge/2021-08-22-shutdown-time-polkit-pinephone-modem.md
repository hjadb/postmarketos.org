title: "Long shutdown time, Polkit, PinePhone Modem"
date: 2021-08-22
---
Recent and current issues in postmarketOS edge:

* Shutdown takes more than 60 seconds, being investigated here:
  [pmaports#1197](https://gitlab.com/postmarketOS/pmaports/-/issues/1197)

* Various issues with polkit / polkit-elogind packages (wrong package getting
  installed, apk refusing to install packages etc.). This has been resolved in
  Alpine:
  [aports#12708](https://gitlab.alpinelinux.org/alpine/aports/-/issues/12708)

* PinePhone Modem regressions, SMS and calls are not arriving while phone is in
  deep sleep. This is about to be resolved by
  [pmaports!2453](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2453),
  see [this comment](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2453#note_657197453)
  for details.
